INTRO

Lag et text adventure-spill i Java [1]. Oppgaveteksten er laget av Stein Llanos & Håvard Espeland.

--------

BAKGRUNN

Du våkner en søndags morgen etter en sen kveld med after-ski og dårlig
musikk. Det er kaldt i hytta, og ikke har du sovet stort heller. Klokken
begynner å bli mye og skal du få brukt heiskortet ditt før det går ut må
du skynde deg ut i bakken, men før den tid må du bli klar.

--------

OPPGAVE

Lag et adventure-spill hvor spilleren har to parametre som
beskriver hans tilstand: våkenhet og kroppstemperatur. For å komme seg ut
i bakken må spillerens våkenhet og kroppstemperatur være over en
grenseverdi du velger selv. 

Hytta er rimelig og har bare 3 - 5 rom, noe som begrenser verdenen han kan
bevege deg i. Grensesnittet for å kontrollere spilleren er alternativer på
skjermen, ikke fri tekst. Det oppfordres til å kombinere interaksjoner for
å få fremgang i spillet, f.eks sette på kaffe før du drikker den.

Du står fri til å gjøre spillet så enkelt eller komplisert du selv vil.

--------

EKSEMPEL

På soverommet står det et garderobeskap.

1) Åpne garderobeskapet.
2) Gå til stua.

> 1

I garderobeskapet ligger det en lue og en stilongs.

1) Ta på lue.
2) Ta på stilongs.
3) Gå til stua.

> 2

Stilongs varmer godt. Du er nå ikke lenger stivfrossen, og bare iskald.

--------

[1] http://en.wikipedia.org/wiki/Interactive_fiction
