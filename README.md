Workshop i Spillprogrammering, høsten 2013
==========================================

# Kort info

Last ned prekoden fra dette Git-repositioriet via `git` eller bruk *download*-linken. Oppgaveteksten finner dere i `OPPGAVETEKST.txt`.
For å kompilere trenger dere Java SE Development Kit (Java SDK). Dette skal 1. klasse allerede ha installert som en del av programmeringsemnet.
Om du ikke har det, så kan det [lastes ned her](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html).

# Kompilering & kjøring

All bygging og kjøring er tiltenkt å gjøres via cmd/terminal. Bygging må gjøres fra src/-mappen.

## Windows

For enklere utviking er det anbefalt at dere legger til Java SDK i Windows sin`Path-variabel`. Da slipper du å måtte jobbe direkte fra Java SDK-mappen.
Du kan finne instruksjoner på dette ved å [klikke her](http://learnedstuffs.wordpress.com/2012/03/14/setting-up-javac-command-on-windows-7/).

***Kompilering***

Kjør følgende kommando:

`.\make.bat`

***Kjøring***

Kjør følgende kommando:

`.\make.bat play`

***Opprydding***

"Rydder opp" etter kompilering. Sletter alle .class-filer. Kjør:

`.\make.bat clean`

## Mac OSX / Linux (Unix)

***Kompilering***

Kjør følgende kommando:

`make`

***Kjøring***
Denne kjører programmet og kompilerer først om det ikke finnes:

`make play`

***Opprydding***

"Rydder opp" etter kompilering. Sletter alle .class- og backup-filer. Kjør:

`make clean`

## Manuelt

***Kompilering***

`javac *.java`

***Kjøring***

`java Game`
