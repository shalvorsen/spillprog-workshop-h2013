/**
 * @file: GameState.java
 * @description: Contains game critical data and state informatiopn.
 *               To be used as a shared object between classes.
 */

// Import the HashMap object for the room list

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

class GameState {
    // Declare member variables
    private boolean gameOver;
    private Location currentRoom;
    private Calendar currentTime = Calendar.getInstance();
    private Calendar timeLimit = Calendar.getInstance();
    private float awakeness;
    private float bodyTemperature;
    // Initialize the room list (hashmap). Each room identified by a String.
    // NB: private!
    private HashMap<String, Location> rooms = new HashMap<String, Location>();
    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    /* Constructor initializes basic game state */
    public GameState() throws ParseException {
        currentTime.setTime(dateFormat.parse("10:00:00"));
        timeLimit.setTime(dateFormat.parse("12:00:00"));
        awakeness = 1.0f;
        bodyTemperature = 34.0f;
        gameOver = false;
    }

    /* Adds a room identified by a String to the room list */
    public void addRoom(String name, Location room) {
        rooms.put(name, room);
    }

    /* Returns a specific room from the list, if it exists.
       Returns null on failure. */
    public Location getRoom(String name) {
        return rooms.get(name);
    }

    /* Print the introduction message. */
    public void playIntroMessage() {
        System.out.println(
                "Du våkner en søndags morgen etter en sen kveld med after-ski og dårlig musikk. " +
                        "Det er kaldt i hytta, og ikke har du sovet stort heller. Klokken begynner å bli " +
                        "mye og skal du få brukt heiskortet ditt før det går ut må du skynde deg ut i " +
                        "bakken, men før den tid må du bli klar."
        );

    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public Location getCurrentRoom() {
        return currentRoom;
    }

    /* Sets the current room. Identified by the room name String */
    public void setCurrentRoom(String name) {
        currentRoom = rooms.get(name);
    }

    public void addTime(int seconds) {
        currentTime.add(Calendar.SECOND, seconds);
    }

    public void runGame() {
        // Update the current room while it's not game over.
        // Game logic is placed in each room's individual
        // update function.
        while (!isGameOver()) {
            // Print the current time and time limits of the game. These can be altered in the
            // GameState's constructor or through two of its public time related member variables.
            System.out.println(
                    "\nKlokken er nå: " + dateFormat.format(currentTime.getTime()) +
                            "\nHeiskortet går ut: " + dateFormat.format(timeLimit.getTime()) + "\n"
            );

            getCurrentRoom().update(this);
        }

    }
}
