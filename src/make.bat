@echo off
set gameName=Game
set javaFiles=%gameName%.java Livingroom.java Bedroom.java GameState.java

IF [%1] == [] (
GOTO compile
) ELSE (
IF [%1] == [play] (
GOTO play
) ELSE (
IF [%1] == [clean] (
GOTO clean
) ELSE (
echo Unknown command
GOTO quit
)))

:compile
javac %javaFiles%
GOTO quit

:play
java %gameName%
GOTO quit

:clean
del *.class

:quit