/**
 * @file: Location.java
 * @description: An interface declaring which functions that needs to be
 * implemented by a "room" in the game.
 */

interface Location {
    /* Updates the state of the room, interacts with user */
    void update(GameState state);

}
