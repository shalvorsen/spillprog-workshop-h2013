/**
 * @file: Bedroom.java
 * @description: Defines the Livingroom and its related game logic.
 */

class Livingroom implements Location {
    /* Update the livingroom state and game logic by using local variables and
       data from the shared GameState object. */
    public void update(GameState state) {
        // Give a status update, and ask the user what to do.
        System.out.println(
                "Du er i stua. Hva vil du gjøre?\n\n" +
                        "1) Gå til soverommet\n2) Avslutt spillet\n"
        );

        // Retrieve user action and act accordingly
        switch (Game.getAction(1, 2)) {
            case 1:
                // Switch to the bedroom.
                state.setCurrentRoom("bedroom");
                //Moving takes time!
                state.addTime(20);
                break;
            case 2:
                // Exit the game
                state.setGameOver(true);
        }
    }
}
