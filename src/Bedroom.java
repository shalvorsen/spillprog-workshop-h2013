/**
 * @file: Bedroom.java
 * @description: Defines the Bedroom and its related game logic.
 */

class Bedroom implements Location {
    /* Update the bedroom state and game logic by using local variables and
       data from the shared GameState object. */
    public void update(GameState state) {
        // Give a status update, and ask the user what to do.
        System.out.println(
                "Du er på soverommet. Hva ønsker du å gjøre?\n\n" +
                        "1) Gå til stua\n2) Vis denne meldingen igjen.\n" +
                        "3) Avslutt spillet.\n"
        );

        // Retrieve user action and act accordingly
        switch (Game.getAction(1, 3)) {
            case 1:
                // Switch to the livingroom.
                state.setCurrentRoom("livingroom");
                
		// Moving takes time!
                state.addTime(20);

                break;
            case 2:
                // Repeat current message (recursion)
                update(state);
                break;
            case 3:
                // Exit the game
                state.setGameOver(true);
        }
    }
}
