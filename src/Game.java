import java.text.ParseException;
import java.util.Scanner;

/**
 * @file: Game.java
 * @description: The main file and entry point of the game.
 */

class Game {
    static Scanner input = new Scanner(System.in);
    // "Shared" object to keep hold of game state variables accessible throughout
    // the entire application.
    static GameState state;

    /* Main; game entry point. Initializes and runs the game (loop) */
    public static void main(String[] args) throws ParseException {
        // Initialize the game state object
        initGame();

        // Play the introduction message.
        state.playIntroMessage();

        state.runGame();
        // Game over or user exit
        System.out.println("Hade!\n");
    }

    /* Initializes the game state object. New game rooms should be added here. */
    public static void initGame() throws ParseException {
        state = new GameState();

        // Create some game rooms
        Bedroom bed = new Bedroom();
        Livingroom living = new Livingroom();

        // Add them to the game state hashmap (list of rooms identified by Strings)
        state.addRoom("bedroom", bed);
        state.addRoom("livingroom", living);

        // Set the startup room
        state.setCurrentRoom("bedroom");
    }



    /* Helper function to retrieve an integer from the user (stdin).
 * The function ensures that the integer is between min-max.
 * Should not be overrided unless necessary.
 */
    public static int getAction(int min, int max) {
        // Set action to an invalid value
        int action = min - 1;

        // Prompt the user to enter a value
        System.out.print("> ");

        // Make sure it's a valid integer
        if (input.hasNextInt())
            action = input.nextInt();
        else
            input.next(); // not a valid integer

        // Check if action is between the provided limits. Loop if not.
        while ((action < min) || (action > max)) {
            // Ops, invalid input!
            System.out.print("Invalid option, must be integer between " +
                    min + " - " + max + ".\n\n> ");

            // Make sure it's a valid integer
            if (input.hasNextInt())
                action = input.nextInt();
            else
                input.next(); // It's not, ask again
        }

        return action;
    }
}
